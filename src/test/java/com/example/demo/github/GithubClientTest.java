package com.example.demo.github;

import com.example.demo.domain.Repositorio;
import java.util.List;
import static org.assertj.core.api.Assertions.assertThat;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class GithubClientTest {

    @Autowired
    private GithubClient githubClient;

    @Test
    public void testRepos() {
        List<Repositorio> repos = githubClient.buscarRepositoriosPorUsuario("spring");
        assertThat(repos.size()).isGreaterThan(20);
        repos.forEach(System.out::println);

    }

}
