package com.example.demo.github;

import com.example.demo.domain.Repositorio;
import java.util.List;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "github", url = "${url.github}")
public interface GithubClient {

    @GetMapping(value = "/users/{usuario}/repos")
    List<Repositorio> buscarRepositoriosPorUsuario(@PathVariable("usuario") String usuario);

}
