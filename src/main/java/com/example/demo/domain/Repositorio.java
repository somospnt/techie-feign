package com.example.demo.domain;

import lombok.Data;
import lombok.ToString;

@ToString
@Data
public class Repositorio {

    private Long id;
    private String name;
    private String description;

}
